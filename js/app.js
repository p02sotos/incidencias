Ember.LOG_BINDINGS = true
App = Ember.Application.create({
	LOG_ACTIVE_GENERATION: true,
    // log when Ember looks up a template or a view
    LOG_VIEW_LOOKUPS: true,
    LOG_TRANSITIONS: true

});
//Cargamos el Store
App.Store = DS.Store.extend({
	revision: 12,
	adapter: DS.RESTAdapter.extend({		
		namespace: 'api'
	})
});
App.Incidencia = DS.Model.extend({
	
	origen: DS.attr('string'),
	motivo: DS.attr('string'),
	comunidad: DS.attr('string'),
	resumen: DS.attr('date'),
	descripcion: DS.attr('string'),
	fechaentrada: DS.attr('string'),
	horaentrada:DS.attr('string'),
	fecharesolucion: DS.attr('string'),	
	realizada:DS.attr('boolean'),
	urgencia:DS.attr('string'),
		

});

App.Router.map(function() {
  	this.route('incidencias',{path: '/incidencias'});
  	this.route('new',{path: '/new'});
	this.resource('incidencia',{ path: '/incidencias/:incidencia_id' }); 
  });
	

App.IncidenciasRoute = Ember.Route.extend({
	model: function(){
		//return $.getJSON('/api/incidencia');
		return this.store.find('incidencia');
	}
});

App.NewRoute = Ember.Route.extend({
	model: function(){
		return {		
	 	origen: '',
		motivo: '',
		comunidad: '',
		resumen: '',
		descripcion: '',
		fechaentrada: '',
		horaentrada:'',
		fecharesolucion: '',	
		realizada:'',
		urgencia:'' 	
		}
	}
});

App.IncidenciaRoute = Ember.Route.extend({
	model: function(params){
		//var incidencias = this.modelFor('incidencias');
		//return incidencias.findBy('id', parseInt(params.incidencia_id));
		return this.store.find('incidencia',params.incidencia_id );
	}
});



App.IncidenciaController = Ember.ObjectController.extend({
	editing: false,
	actions: {
		clickRow: function(){
			alert("pulsado");
		},
		edit: function(){
			this.set('editing', true);
		},
		doneEditing: function(){
			var valid = $('#editForm').parsley().validate();			
			if (!valid){				

			}else {
				var incidencia = this.get('model');
				Ember.Logger.log('log value of foo:', incidencia);				
				incidencia.save();
				this.set('editing', false);
			}
			
		}
	}
});
App.RowView = Ember.View.extend({
    tagName: 'tr',
    self: this,

    click: function(evt) {
    	var rowClicked = this.get('content');

        Ember.Logger.log('Evento:',rowClicked);
        Ember.Logger.log('ID:',rowClicked.id);
        this.get('controller').transitionToRoute('incidencia',rowClicked.id);


       
    }
});

App.NewController = Ember.ObjectController.extend({
	needs: ['incidencias'],
	actions: {
		save: function(){
			console.log('ejecutado save');		
			var valid = $('#newForm').parsley().validate();
			if (!valid){				

			}else {
				var incidencia = this.get('model');
				Ember.Logger.log('log value of foo:', incidencia);
				var inc = this.store.createRecord('incidencia', incidencia);
				inc.save();
				this.transitionToRoute('incidencias');
			}
			
				
				//incidencias = this.get('controllers.incidencias').get('model');
				//incidencias.unshiftObject(incidencia);
				//inc.save();
				

			}
		
	},
});

Ember.TextField.reopen({     
	attributeBindings: [ 'data-parsley-value', 'data-parsley-group', 'data- parsley-multiple','data-parsley-validate- if-empty','data-parsley-required', 'data-parsley-type', 'data-parsley- minlength', 'data-parsley-maxlength', 'data-parsley-length', 'data-parsley-min', 'data-parsley-max', 'data-parsley-range', 'data-parsley-pattern','data-parsley-mincheck', 'data-parsley-maxcheck', 'data-parsley-check', 'data-parsley-equalto']
 });

App.NewView = Ember.View.extend({

	didInsertElement: function (){
		

	}
});

