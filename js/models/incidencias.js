
App.Incidencia = DS.Model.extend({
	
	origen: DS.attr('string'),
	motivo: DS.attr('string'),
	comunidad: DS.attr('string'),
	resumen: DS.attr('date'),
	descripcion: DS.attr('string'),
	fechaentrada: DS.attr('string'),
	horaentrada:DS.attr('string'),
	fecharesolucion: DS.attr('string'),	
	realizada:DS.attr('boolean'),
	urgencia:DS.attr('string')    	

});

/*App.Incidencia.FIXTURES = [
	{
		id: 1,
	 	description: 'Se ha roto un bajante',
	 	type: 'Llamada',
	 	comunidad: 'FATIMA 66',
	 	origin: 'Vecina del cuarto D',
	 	phone: '957404166',
	 	createdAt: '12/05/2014'
	},
	{
		id: 2,
	 	description: 'Hay cagadas de perro en el portal',
	 	type: 'Presencial',
	 	comunidad: 'ARCOS08',
	 	origin: 'Vecina del cuarto J',
	 	phone: '957555555',
	 	createdAt: '11/05/2014'



	}
]*/